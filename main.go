package main

import (
	"flag"
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
	)

func main() {

	// parse command line arguments
	configPtr := flag.String("config", "config.yaml", "A path to the YAML configuration file.")
	verbosePtr := flag.Bool("verbose", false, "Be more verbose during execution.")
	flag.Parse()

	// read in config file
	configData, err := os.ReadFile(*configPtr)
	if(err != nil) { 
		fmt.Print("Error reading configuration file: ")
		panic(err)
		}

	config := make(map[interface{}]interface{})
	err = yaml.Unmarshal([]byte(configData), &config)
	if(err != nil) { 
		fmt.Print("Error reading configuration file: ")
		panic(err) 
		}

	if( *verbosePtr ) {
		fmt.Println("Verbosity increased.")
		fmt.Println("Config file contains...")
		fmt.Println(configData)
		}

	// create all modules
	// build execution tree 
	// launch it!

    fmt.Println("This will be the heart of the executable.")
}
