# User Agent Filter

The TL;DR: while Mastodon doesn't support unrestricted search without patches, other fediverse servers do and yet they share the same network.
Because there's no central authortity, there's no central list of instances to block in order to preserve search visibility.
These servers do identify themselves by their user agent when they connect, however, and since all connections are handled via a web server we can use the web server logs to detect them.
This program is designed to do that sort of detection, and possibly more.
